# Presentation Travaux pratique - recherche et innovation

## Presentation

Demonstration d'un CI/CD basique 

## Requirements

- Ubuntu 16.04 LTS

## Contributing

- Valentin Etchevarne
- Kevin Lous

## Software stack

- Vagrant 2.2.6
- Nextcloud (version snap)
- Snapd

## Getting help

Please see email us at k{dot}lous{at}cfa-afti{dot}fr or v{dot}etchevarne{at}cfa-afti{dot}fr
