**Note:** Ce fichier est généré automatiquement.

## 1.1.1

### Fixed (7 changement)

- Ajout du fichier .gitlab-ci.yml pour l'intégration continue.
- Déploiement d'une machine virtuelle "Nextcloud" sur virtualbox grace à Vagrant.
- Configuration de la machine virtuelle pour installer le service nextcloud.
- Ajout d'un fichier Vagranfile, qui permet à Vagrant de se connecter à Virtualbox et de créer une machine virtuelle Nextcloud.
- Ajout d'un fichier README afin d'expliquer le projet.
- Ajout d'un fichier CONTRIBUTING pour présenter les contributeurs de ce projet.
- Ajout d'un fichier CHANGELOG pour identifier l'ensemble des modifications du projet.
